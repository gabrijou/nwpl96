# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 15:14:03 2020

@author: gabriel.jouan
"""
import pickle
import modelL96
import numpy as np
import concurrent.futures
import time
import multiprocessing
import os

""" L96 simulation function """

def SimuL96(kwargs):
    Model = modelL96.L96(**kwargs)
    Model.iterate(kwargs['time'])
    return Model.history.X

def SimuEnsL96(kwargs):
    ensModel = modelL96.EnNWPL96(**kwargs)
    ensModel.iterate(kwargs['time'])
    return ensModel.history

    
if __name__ == "__main__":
    Test =True
    Simu = True
    
    if Simu:
        ####################### SIMULATION PART #########################
        
        #For spyder users to CPU-multiprocessing
        __spec__ = "ModuleSpec(name='builtins', loader=<class '_frozen_importlib.BuiltinImporter'>)"
       
        # Intervals time initial trajectory
        save_dt = 50 # MTU (model time unit)
        dt_True = 0.001
        
        # Number of individu
        nT = 50007
        
        # Convert nT following save_dt
        nInit = int(nT*(save_dt)+1)
        path = "./res/"
        prefix='TEST_'
        
        # Parameters of model
        kwargs = {'ind':1,'M':20,'J':32, 'K':8, 'h':1, 'F':20, 'c':10, 'b':10,'time':nInit, 
                        'save_dt':save_dt,'noprog':True,'multiprocess':True}
        
        # Print model iteration
        if nT < 1000:
            kwargs['noprog'] = False
        
    
        # Initialisation seed
        seed = 123
        np.random.seed(seed)
    
        # Model ens init L96   with DelatT = 0.15 MTU (1 MTU = 5 days)
        ModelEnsInit = modelL96.L96(**kwargs)
        ModelEnsInit.iterate(kwargs['time'])
        
        # Number of individu cut in nbatch avoiding memory crash
        batch= True
        nbacth = 100
        lenSeq = int(nT/nbacth)
        
        h=0
        for k  in range(nbacth):
            nind = range(h,(lenSeq+h))
            store = dict()
            store['initL96_time'] = np.array(nind)
            store['initL96_X'] = ModelEnsInit.history.X.values[nind,:]
            store['initL96_Y'] = ModelEnsInit.history.Y.values[nind,:]
            pickle.dump(store, open(os.path.join(path,prefix+'initL96_'+str(k)+'.p'), "wb" ))
            h = h+lenSeq
        
        # Free memory space
        del ModelEnsInit
        
        kwargs['time']=3
        
        # Set interval save point following lead time (0.2 MTU = 1 day)
        kwargs['save_dt']=0.2
        kwargs['noprog'] = True
        
        # Simulate Ensemble NWP and observation trajectpries for each batch of initial points.
        for k  in range(nbacth):
            
            nind = range(h,(lenSeq+h))
            init_dict = pickle.load(open(os.path.join(path,'initL96_'+str(k)+'.p'), "rb" ))
            nT_batch = init_dict['initL96_X'].shape[0]
            
    
            kwargs_tmp =[]
            for i in range(init_dict['initL96_X'].shape[0]):
                tmp_dict = {**kwargs,'X_init':init_dict['initL96_X'][i,:]}
                kwargs_tmp.append(tmp_dict)
           
            
            # ENSEMBLE MODEL L96 with DelatT = 0.2 MTU
            store = dict()
            
            kwargs['multiprocess'] = True
            
            if kwargs['multiprocess']:
                print("\nBatch: %d, Starting Ensemble Forecasts Multiprocess, members= %d" % (k,kwargs["M"]))
                max_workers = 6
                
                start_processes = time.perf_counter()
                with multiprocessing.Pool(processes=max_workers) as pool:
                    
                    # Ensemble trajectory with 3 MTU
                    results = pool.map(SimuEnsL96, kwargs_tmp,chunksize=int(len(kwargs_tmp)/max_workers))
                    store['EnsModel']= results
                    
                    # Observation trajectory with 3 MTU
                    results = pool.map(SimuL96, kwargs_tmp,chunksize=int(len(kwargs_tmp)/max_workers))
                    store['ObsModel']= results
                print("\nSimulationtime : "+str(time.perf_counter()-start_processes))
            else :
                print("\nBatch: %d, Starting Ensemble Forecasts Multiprocess, members= %d" % (k,kwargs["M"]))
                start_processes = time.perf_counter()
                h=0
                for i in map(SimuEnsL96, kwargs_tmp):
                    if h % 10 == 0:
                        print('iter: %d' % h)
                    res = list(i)
                    store['EnsModel'].append(i)
                    h+=1
                h=0
                for i in map(SimuL96, kwargs_tmp):
                    if h % 10 == 0:
                        print('iter: %d' % h)
                    res = list(i)
                    store['ObsModel'].append(i)
                    h+=1
                print("\nSimulationtime : "+str(time.perf_counter()-start_processes))
            store['nInd'] = nind
            store['seed'] = seed 
            store['initL96'] = init_dict
            
            pickle.dump(store, open(os.path.join(path,prefix+'EnsL96_'+str(k)+'.p'), "wb" ))
            
        del results
        del store