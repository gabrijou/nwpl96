# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 15:14:03 2020

@author: gabriel.jouan
"""

import modelL96
import numpy as np
import time
from multiprocessing import Pool
from ranky import rankz
import matplotlib.pyplot as plt
import seaborn as sns

""" L96 simulation function """

def SimuL96(kwargs):
    Model = modelL96.L96(**kwargs)
    Model.iterate(kwargs['time'])
    return Model.history

def SimuEnsL96(kwargs):
    ensModel = modelL96.EnNWPL96(**kwargs)
    ensModel.iterate(kwargs['time'])
    return ensModel.history

if __name__ == "__main__":

    ####################### SIMULATION PART #########################
    
    # WARNING
    #For spyder users, if you ant to use CPU-multiprocessing
    # go to tools > preferences:
    # Run > Configuration per file > Execute in an external system terminal
    _spec_ = "ModuleSpec(name='builtins', loader=<class '_frozen_importlib.BuiltinImporter'>)"
    
    # Intervals time initial trajectory
    save_dt = 0.15 # MTU (model time unit)
    dt_True = 0.001
    
    # Number of individu
    nT = 500
    
    # Convert nT following save_dt
    nInit = int(nT*(save_dt)+1)
    path = "./"
    
    # Parameters of model, M=20 members
    kwargs = {'ind':1,'M':20,'J':32, 'K':8, 'h':1, 'F':20, 'c':10, 'b':10,'time':nInit, 
                    'save_dt':save_dt,'noprog':True,'multiprocess':True}
    
    
    # Print model iteration
    if nT < 1000:
        kwargs['noprog'] = False
    

    # Initialisation seed
    seed = 231
    np.random.seed(seed)
    
    # Model L96 Initialisation for NWP forecast
    ModelOBSInit = modelL96.L96(**kwargs)
    ModelOBSInit.iterate(kwargs['time'])
    
    # NWP L96 parameters:
    kwargs['save_dt'] = 0.2     #point intervals in MTU
    kwargs['time']  = 3     #lead time in MTU (15 days = 3 MTU)
    

            
    # NWP initialisation
    kwargs_tmp =[]
    for i in range(ModelOBSInit.history.X.values.shape[0]):
        tmp_dict = {**kwargs,'X_init':ModelOBSInit.history.X.values[i,:]}
        kwargs_tmp.append(tmp_dict)
   
    # ENSEMBLE MODEL L96 with DelatT = 0.2 MTU
    store = dict()
    k = 1
    
    
    if kwargs['multiprocess']:
        print("\nBatch: %d, Starting Ensemble Forecasts Multiprocess, members= %d" % (k,kwargs["M"]))
        max_workers = 6
        
        start_processes = time.perf_counter()
        pool = Pool(processes=max_workers)
            
        # Ensemble trajectory with 3 MTU
        results = pool.map(SimuEnsL96, kwargs_tmp,chunksize=int(len(kwargs_tmp)/max_workers))
        store['EnsModel']= results
        
        # Observation trajectory with 3 MTU
        results = pool.map(SimuL96, kwargs_tmp,chunksize=int(len(kwargs_tmp)/max_workers))
        store['ObsModel']= results
        print("\nSimulationtime : "+str(time.perf_counter()-start_processes))
    else :
        store['EnsModel'] = list()
        store['ObsModel'] = list()
        print("\nBatch: %d, Starting Ensemble Forecasts Multiprocess, members= %d" % (k,kwargs["M"]))
        start_processes = time.perf_counter()
        
        # Ensemble trajectory with 3 MTU
        h=0
        for i in map(SimuEnsL96, kwargs_tmp):
            if h % 10 == 0:
                print('iter: %d' % h)
            res = list(i)
            store['EnsModel'].append(i)
            h+=1
            
        # Observation trajectory with 3 MTU
        h=0
        for i in map(SimuL96, kwargs_tmp):
            if h % 10 == 0:
                print('iter: %d' % h)
            res = list(i)
            store['ObsModel'].append(i)
            h+=1
        print("\nSimulationtime : "+str(time.perf_counter()-start_processes))
    store['seed'] = seed 
    store['initL96'] = ModelOBSInit.history.X.values

    #Set horizon to study
    horizon = 7
    
    # Observation and esemble from X1 and Energy    
    obs = np.array([store['ObsModel'][i].X.values[horizon,0] for i in range(nT)])
    obs_E = np.array([np.mean(store['ObsModel'][i].X.values[horizon,:]**2) for i in range(nT)])
    
    X1_t1 = np.empty((nT,kwargs['M']))
    E_t1 = np.empty((nT,kwargs['M']))
    for i in range(nT):
        ens = []
        ens_E = []
        for m in range(kwargs['M']):
            ens.append(store['EnsModel'][i]['X_'+str(m)][horizon,0])
            ens_E.append(np.mean(store['EnsModel'][i]['X_'+str(m)][horizon,:]**2))
        X1_t1[i,:] = ens
        E_t1[i,:] = ens_E


    # Plot of the intervariable structure at fixed time
    x_theta = [2*np.pi /kwargs['K'] * i for i in range(kwargs['K']+1)]
    y_theta = [2*np.pi /(kwargs['K']*kwargs['J'])  * i for i in range(kwargs['K']*kwargs['J']+1)]
    
    def add_point(x):
        return np.append(x.values, x[0])
    fig = plt.figure(figsize=(6, 6))
    # ax1 = plt.axes([0, 0, 0.4, 1], projection='polar')
    ax1 = fig.add_subplot(111, projection='polar')
    ax1.plot(x_theta, add_point(store['ObsModel'][0].X[-1]), lw=3, zorder=10, label='L96 X')
    ax1.plot(x_theta, add_point(store['EnsModel'][0].X_0[-1]), lw=3, zorder=10, label='NWP X')
    ax1.plot(y_theta, add_point(store['ObsModel'][0].Y[-1])*10, lw=3, label='L96 Y')
    ax1.set_rmin(-14); ax1.set_rmax(14)
    l = ax1.set_rgrids([-7, 0, 7], labels=['', '', ''])[0][1]
    l.set_linewidth(2)
    ax1.set_thetagrids([])
    ax1.set_rorigin(-22)
    ax1.legend(frameon=False, loc=1);
    plt.title('Intervariable structure of the L96 model and numerical approximation at 3 MTU.')
    plt.savefig('./image/L96Cycle.png')
    plt.show()

    # Construction of regime A and B based on covariance Allen 2020
    cov = []
    pad=6
    for i in range(pad+1,nT):
        cov_tmp = []
        for k in range(int(kwargs['K']/2)):
            xj = []
            xj_2 = []
            for t in range(pad+1):
                xj.append(store['ObsModel'][i-t].X.values[horizon,k])
                xj_2.append(store['ObsModel'][i-t].X.values[horizon,k+int(kwargs['K']/2)])
            cov_tmp.append(np.cov(xj,xj_2)[0,1])
        cov.append(sum(cov_tmp))
    cov_all = cov


    regime = np.array(cov_all)>0
    
    # TIME PERSITENCY A and B ANALYSIS 
    tim_persit_A = []
    tim_persit_B = []
    i=0
    while i < len(regime):
        flag=True
        j=1
        tim_persit=1
        while flag:
            if (i+j) < len(regime):
                if regime[i]==regime[i+j]:
                    tim_persit=tim_persit+1
                    j=j+1
                else:
                    flag=False
            else:
                flag=False
        if regime[i]:
            tim_persit_A.append(tim_persit)
        else:
            tim_persit_B.append(tim_persit)
        i=i+j
    MTU = 5
    # PDF TIME PERSITENCY
    plt.hist(np.array(tim_persit_A)/MTU)
    plt.show()
    plt.hist(np.array(tim_persit_B)/MTU)
    plt.show()
    
    # MEAN TIME IN MTU
    print(np.array(tim_persit_A).mean()/(MTU))
    print(np.array(tim_persit_B).mean()/(MTU))
    
    regime_A = np.array(cov_all)>0
    regime_B = np.array(cov_all)<=0
    
    print(sum(regime_A)/(nT-(pad+1))*100)
    print(sum(regime_B)/(nT-(pad+1))*100)
    
    obs_copy = obs[(pad+1):]
    obs_copy = obs_copy[np.newaxis].T
    ensemble = X1_t1[(pad+1):,:]
    ensemble = ensemble[np.newaxis].T

    result = rankz(obs_copy, ensemble, regime_A[np.newaxis].T*1)
    
    plt.bar(range(1,ensemble.shape[0]+2), result[0]/sum(result[0]))
    plt.xlabel('rank')
    plt.title('Rankhistogram at horizon 7 of ensemble and observation from regime A.')
    plt.savefig('./image/rankHistA.png')
    plt.show()
   
    
    # feed into rankz function
    result = rankz(obs_copy, ensemble, regime_B)
    
    plt.bar(range(1,ensemble.shape[0]+2), result[0]/sum(result[0]))
    plt.xlabel('rank')
    plt.title('Rankhistogram at horizon 7 of ensemble and observation from regime B.')
    plt.savefig('./image/rankHistB.png')
    plt.show()
    
    
    # Plot first 100 points of obs, X_obs covariance, regime and ensemble at settled horizon.
    time_len = 365
    fig, ax = plt.subplots( nrows=1, ncols=1 ) 
    plt.subplot(411)
    plt.plot(obs[(pad+1):(time_len+(pad+1))])
    plt.plot(X1_t1.mean(axis=1)[(pad+1):(time_len+(pad+1))])
    plt.legend(['obs','ens.mean'])
    plt.xticks([])
    plt.ylabel('X1')
    plt.title('One year simulated sequence of observation and NWP L96.')
    
    plt.subplot(412)
    plt.plot(obs_E[(pad+1):(time_len+(pad+1))])
    plt.xticks([])
    plt.ylabel('E')

    plt.subplot(413)
    plt.plot(cov[:time_len])
    plt.xticks([])
    plt.ylabel('Covariance')

    plt.subplot(414)
    plt.plot(regime[:time_len])
    plt.xlabel('time')
    plt.ylabel('Regime')
    plt.yticks([0,1],['B','A'])
    plt.savefig('./image/TimesSeries.png')
    plt.show()
    
    
    obs_copy = obs[:(nT-pad-1)]
    sns.kdeplot(obs_copy[regime_A], bw=0.5)
    sns.kdeplot(obs_copy[regime_B], bw=0.5)
    plt.xlabel('X1')
    plt.title('X1 observation distribution by regimes.')
    plt.legend(['A','B'])
    plt.savefig('./image/ObsRegimeDistributionX1.png')
    plt.show()
    
    obs_copy_E = obs_E[:(nT-pad-1)]
    sns.kdeplot(obs_copy_E[regime_A], bw=0.5)
    sns.kdeplot(obs_copy_E[regime_B], bw=0.5)
    plt.legend(['A','B'])
    plt.xlabel('E')
    plt.title('Energy observation distribution by regimes.')
    plt.savefig('./image/ObsRegimeDistributionE.png')
    plt.show()
    
   
