# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 13:57:40 2020

@author: gabriel.jouan
"""
import os
import pickle
import numpy as np
import json 
os.chdir("F:/THESE/code/KAGGLE/SimuL96/")
if __name__ == "__main__":
    ################ TEST RESULTS #####################
    nbatch = 10
    path_res = "./res/"

    data_train = dict()
    for k in range(nbatch):
        print("Batch loading: %d" % k)
        store = pickle.load(open(os.path.join(path_res,'EnsL96_'+str(k)+'.p'), "rb" ))
        nHorizon = store['ObsModel'][0].shape[0]
        nVar = store['ObsModel'][0].shape[1]
        nT = len(store['ObsModel'])
        nMember = len(store['EnsModel'][0].variables)-2
        
        data_train_tmp = dict()
        
        for j in range(nVar):
            for h in range(nHorizon):
                data_train_tmp['Obs_X'+str(j)+'_'+str(h)] = np.array([store['ObsModel'][i].values[h,j] for i in range(nT)])
                
                ens = np.empty((nT,nMember))
                for i in range(nT):
                    for m in range(nMember):
                        ens[i,m] = store['EnsModel'][i]['X_'+str(m)][h,j]
                data_train_tmp['Ens_X'+str(j)+'_'+str(h)] = ens
       
        if k==0:
            data_train = data_train_tmp
        else:
            for l in data_train.keys():
                data_train[l] = np.concatenate((data_train[l],data_train_tmp[l]))
    data_train_tmp = data_train.copy() 
    for l in data_train.keys():
        data_train[l] = data_train[l].tolist()
    path_save = './data_json/'
    with open(os.path.join(path_save,'DataTrainEnsL96.json', 'w')) as fp:
        json.dump(data_train, fp)