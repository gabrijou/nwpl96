# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 10:12:03 2020

@author: gabriel.jouan


    L96-Visualization/L96.py /
    @raspstephan
    raspstephan Initial commit
    Latest commit b57d0b1 on 11 Jul 2019
     History
     1 contributor
    225 lines (195 sloc)  8.34 KB
    
    Definition of the Lorenz96 model.
    Created on 2019-04-16-12-28
    Author: Stephan Rasp, raspstephan@gmail.com
"""

import sys
import numpy as np
import xarray as xr
from tqdm import tqdm




class L96(object):
    def __init__(self,dt=0.001, X_init=None, Y_init=None,**kwargs):
        # Model parameters
        self.dt = dt
        if kwargs is not None:
            for key, value in kwargs.items():
                setattr(self, key, value)
        self.step_count = 0
        self.save_steps = int(self.save_dt / self.dt)
        self.X = np.random.rand(self.K) if X_init is None else X_init.copy()
        self.Y = np.zeros(self.J * self.K) if Y_init is None else Y_init.copy()
        self._history_X = [self.X.copy()]
        self._history_B = [-self.h * self.c * self.Y.reshape(self.K, self.J).mean(1)]
        self._history_Y = [self.Y.copy()]

    def _rhs_X_dt(self, X, Y=None, B=None):
        """Compute the right hand side of the X-ODE."""
        if Y is None:
            dXdt = (
                    -np.roll(X, -1) * (np.roll(X, -2) - np.roll(X, 1)) -
                    X + self.F + B
            )
        else:
            dXdt = (
                    -np.roll(X, -1) * (np.roll(X, -2) - np.roll(X, 1)) -
                    X + self.F - self.h * self.c * Y.reshape(self.K, self.J).sum(1)/self.b
            )
        return self.dt * dXdt

    def _rhs_Y_dt(self, X, Y):
        """Compute the right hand side of the Y-ODE."""
        dYdt = (
                       -self.b * np.roll(Y, -1) * (np.roll(Y, -2) - np.roll(Y, 1)) -
                       Y + self.h / self.b * np.repeat(X, self.J)
               ) * self.c
        return self.dt * dYdt

    def _rhs_dt(self, X, Y):
        return self._rhs_X_dt(X, Y=Y), self._rhs_Y_dt(X, Y)
    
    def step_euler(self, add_B=True, B=None):
        """Integrate one time step with EULER"""
        dX, dY = self._rhs_dt(self.X, self.Y)
        self.X += dX
        self.Y += dY
        self.step_count += 1
        if self.step_count % self.save_steps == 0:
            self._history_X.append(self.X.copy())
            self._history_Y.append(self.Y.copy())
                
    def step_RK4(self, add_B=True, B=None):
        """Integrate one time step with RK4"""

        k1_X, k1_Y = self._rhs_dt(self.X, self.Y)
        k2_X, k2_Y = self._rhs_dt(self.X + k1_X / 2, self.Y + k1_Y / 2)
        k3_X, k3_Y = self._rhs_dt(self.X + k2_X / 2, self.Y + k2_Y / 2)
        k4_X, k4_Y = self._rhs_dt(self.X + k3_X, self.Y + k3_Y)

        self.X += 1 / 6 * (k1_X + 2 * k2_X + 2 * k3_X + k4_X)
        self.Y += 1 / 6 * (k1_Y + 2 * k2_Y + 2 * k3_Y + k4_Y)

        self.step_count += 1
        if self.step_count % self.save_steps == 0:
            self._history_X.append(self.X.copy())
            self._history_Y.append(self.Y.copy())

    def iterate(self, time):
        steps = int(time / self.dt)
        for n in tqdm(range(steps), disable=self.noprog):
            #self.step_euler()
            self.step_RK4()

    @property
    def state(self):
        return np.concatenate([self.X, self.Y])

    def set_state(self, x):
        self.X = x[:self.K]
        self.Y = x[self.K:]

    @property
    def parameters(self):
        return np.array([self.F, self.h, self.c, self.b])

    def erase_history(self):
        self._history_X = []
        self._history_Y = []

    @property
    def history(self):
        dic = {}
        dic['X'] = xr.DataArray(self._history_X, dims=['time', 'x'], name='X')

        dic['X_repeat'] = xr.DataArray(np.repeat(self._history_X, self.J, 1),
                               dims=['time', 'y'], name='X_repeat')
        dic['Y'] = xr.DataArray(self._history_Y, dims=['time', 'y'], name='Y')
        return xr.Dataset(
            dic,
            coords={'time': np.arange(len(self._history_X)) * self.save_dt, 'x': np.arange(self.K),
                    'y': np.arange(self.J * self.K)}
        )

    
    
class NWPL96(object):
    def __init__(self,dt=0.005,parameterization=None,**kwargs):
        self.dt = dt
        if kwargs is not None:
            for key, value in kwargs.items():
                setattr(self, key, value)
        self.params = [self.F]
        if self.X_init.shape[0] != self.K:
            raise NameError('K different of X variable length')
            
        self.X = self.X_init.copy()
        self.save_steps = int(self.save_dt / self.dt)
        self.step_count =0
        self._history_X = [self.X.copy()]
        

    def _rhs_X_dt(self, X):
        
        """Compute the right hand side of the X-ODE."""
        dXdt = (-np.roll(X, -1) * (np.roll(X, -2) - np.roll(X, 1)) -
                X + self.F + self._rhs_U(X))
        return dXdt
        
    def _rhs_U(self,X):
        ## WILLIAMS et al. 2016 parameters (polynomial regression between hc/b*sum_k(Y_jk) = U_j)
        # U_X = 0.262-1.262*X+0.04608*(X**2)+0.007496*(X**3)-0.0003226*(X**4)
        
        # ALLEN et al. 2016 parameters (polynomial regression between hc/b*sum_k(Y_jk) = U_j)
        U_X = -(0.209+1.45*X-0.0127*(X**2)-0.00728*(X**3)+0.000312*(X**4))
        return U_X
    
    def step_euler(self):
        """Step forward one time step with EULER."""
        X_up =  self.X + self.dt * self._rhs_X_dt(self.X)
        self.X = X_up
        self.step_count += 1
        if self.step_count % self.save_steps == 0:
            self._history_X.append(self.X.copy())
    
    def step_RK4(self):
        """Step forward one time step with RK4."""
        k1 = self.dt * self._rhs_X_dt(self.X)
        k2 = self.dt * self._rhs_X_dt(self.X + k1 / 2)
        k3 = self.dt * self._rhs_X_dt(self.X + k2 / 2)
        k4 = self.dt * self._rhs_X_dt(self.X + k3)
        self.X += 1 / 6 * (k1 + 2 * k2 + 2 * k3 + k4)
        
        self.step_count += 1
        if self.step_count % self.save_steps == 0:
            self._history_X.append(self.X.copy())
        
    def iterate(self, time):
        steps = int(time / self.dt)
        for n in tqdm(range(steps), disable=self.noprog):
            #self.step_euler()
            self.step_RK4()
            
    @property
    def state(self):
        return self.X

    def set_state(self, x):
        self.X = x

    @property
    def parameters(self):
        return np.atleast_1d(self.F)

    def erase_history(self):
        self._history_X = []

    @property
    def history(self):
        da = xr.DataArray(self._history_X, dims=['time', 'x'], name='X')
        return xr.Dataset(
            {'X': da},
            coords={'time': np.arange(len(self._history_X)) * self.save_dt, 'x': np.arange(self.K)}
        )



class EnNWPL96(object):
    def __init__(self,**kwargs):
        
        if kwargs is not None:
            for key, value in kwargs.items():
                setattr(self, key, value)
                
        if self.X_init.shape[0] != self.K:
            raise NameError('K different of X variable length')
            
        self.X_ens = np.array(list(map(lambda x: np.random.normal(loc=x,scale=0.1,size=self.M),self.X_init))).T
        self.models = list()
        for m in range(self.M):
            kwargs['X_init'] = self.X_ens[m,:]
            model = NWPL96(**kwargs)
            self.models.append(model)

        
    def iterate(self, time):
        for m in range(self.M):
            self.models[m].iterate(time)
            
    @property
    def history(self):
        dataset = xr.Dataset(coords={'time': np.arange(len(self.models[0].history.X,)) * self.save_dt, 'x': np.arange(self.K)})
        for m in range(self.M):
            da = xr.DataArray(self.models[m].history.X, dims=['time', 'x'], name='X_'+str(m))
            dataset['X_'+str(m)] = da
        return dataset



