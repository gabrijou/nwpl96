# NWPL96

Adaptation of the Lorenz 96 atmospheric toy model to simulate numerical weather model approximation and ensemble forecast. Code was inspired by [S. Rasp](https://github.com/raspstephan), for more explaination about L96 model and how incorporate machine learning process in atmoshperic model visit his website.

# Model equations
Two level model L96 equations:
<div class="image-wrapper" >
    <img src="https://render.githubusercontent.com/render/math?math=\frac{dX_k}{dt}=-X_{k-1}(X_{k-2}-X_{k%2B1})-X_k%2BF-\frac{hc}{b}\sum^{J}_{j=1}Y_{jk}"/>
  </a>
      <p class="image-caption">Equation 1: Large scale ODE.</p>
</div>
<div class="image-wrapper" >
    <img src="https://render.githubusercontent.com/render/math?math=\frac{dY_{j,k}}{dt}=-cbY_{j%2B1,k}(Y_{j%2B2,k}-Y_{j-1,k})-cY_{j,k}%2B\frac{hc}{b}X_{k}"/>
  </a>
      <p class="image-caption">Equation 2: Fast moving ODE.</p>
</div>

The goal of this project is the deploiement of the numerical approach of the L96 model described in [DS. Wilks et al. 2006](https://rmets.onlinelibrary.wiley.com/doi/pdf/10.1017/S1350482706002192) to simulate ensemble forecast at different lead time.

<div class="image-wrapper" >
    <img src="https://render.githubusercontent.com/render/math?math=\frac{dX_k}{dt}=-X_{k-1}(X_{k-2}-X_{k%2B1})-X_k%2BF-U(X_k)"/>
  </a>
      <p class="image-caption">Equation 3: Numerical approximation of L96.</p>
</div>

<div class="image-wrapper" >
    <img src="https://render.githubusercontent.com/render/math?math=U(X_k)=\Beta_0%2B\Beta_1X_k%2B\Beta_2X_k^2%2B\Beta_3X_k^3%2B\Beta_4X_k^4"/>
  </a>
      <p class="image-caption">Equation 4: Polynomial approximation of the resolved variable.</p>
</div>

Following the study of [S. Allen et al. 2019](https://rmets.onlinelibrary.wiley.com/doi/pdf/10.1002/qj.3638),  the cyclic behavior of Lorenz 96 variable embedded regimes behavior. These regimes can be defined by the variable covariance:
<div class="image-wrapper" >
    <img src="https://render.githubusercontent.com/render/math?math=\sum^{\frac{K}{2}}_{k=1}cov(X_k,X_{k%2B\frac{K}{2}})"/>
  </a>
      <p class="image-caption">Equation 5: Regime definition.</p>
</div>
Covariances are estimated accross times series of length 1 MTU (model time unit, 5 days in equivalent time) before the studied time. Regimes are named A for positiv covariance and B else.

# Model information
Parameters of the L96 model and numerical approximation coefficients are set following [S. Allen et al. 2019](https://rmets.onlinelibrary.wiley.com/doi/pdf/10.1002/qj.3638). Also, the number of large scale variables is defined to K=8, fast moving variables J=32, forcing term is defined to F=20 and other constants are set h=1, b=10 and c=10. L96 ODE's are solved following RK4 scheme with dt=0.001 and numercial approximation is solved with the same scheme with dt=0.005. Trajectories are set to 3 MTU by default with 0.2 MTU corresponds to one day simulated. Ensemble or NWP models are initialised using L96 starting points with time intervals set to 0.15 MTU. These points are simulating a lag-1 autocorrelation of 0.5 for each variables, equivalent to daily analysis NWP initialisation. Ensemble model of M members are also initialisaed with a Normal law ![](https://render.githubusercontent.com/render/math?math=X^{m}_{k}~\mathcal{N}(X_k,0.1^2)+,+\forall+m+\in\\{1,...,M\\}).

# Model verification plots
All plots are made from the "main.py" with parameters from the part above and a number of points nT = 500 taking 760 sec (12 min)  approximately to be created using multiprocessing CPU (core I7). 

If you want just simulate data base with multiprocessing and RAM preservation, the "dataSimu.py + BatchToJson.py" will do the job.

First, we have the circular plot structure of the evolution of the L96 and his numerical approximation variable space at fixed time. In blue, we have the 8 resolved variables X and in green the fast moving 256 (8*32) unresolved variables Y. The yellow line represent the linear numerical approximation of X variables of L96.
<div class="image-wrapper" >
    <img src="/image/L96Cycle.png" alt=""/>
  </a>
      <p class="image-caption">Figure 1: Model evolution at fixed time.</p>
</div>


In the following plot regimes are estimated from observation serie and ensemble forecast at 7 days of forecasting range (1.4 MTU). 

<div class="image-wrapper" >
    <img src="/image/TimesSeries.png" alt=""/>
  </a>
      <p class="image-caption">Figure 2: Times series with ensemble mean forecast at 7 days.</p>
</div>

<div class="image-wrapper" >
    <img src="/image/ObsRegimeDistributionX1.png" alt=""/>
  </a>
      <p class="image-caption">Figure 3: Observation distribution by regimes.</p>
</div>

<div class="image-wrapper" >
    <img src="/image/ObsRegimeDistributionE.png" alt=""/>
  </a>
      <p class="image-caption">Figure 4: Observation distribution by regimes.</p>
</div>

<div class="image-wrapper" >
    <img src="/image/rankHistA.png" alt=""/>
  </a>
      <p class="image-caption">Figure 5: RankHistogram of observation and ensemble forecast at 7 days of forecasting range and linked to the A regime.</p>
</div>

<div class="image-wrapper" >
    <img src="/image/rankHistB.png" alt=""/>
  </a>
      <p class="image-caption">Figure 6: RankHistogram of observation and ensemble forecast at 7 days of forecasting range and linked to the B regime.</p>
</div>